import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import Grid, { GridProps } from "./Grid";
import Wrapper from "../Wrapper/Wrapper";

export default {
  title: "Components/Layout/Grid",
  component: Grid
} as Meta;

const Template: Story<GridProps> = (args) =>
<Wrapper size={'medium'}>
  <Grid {...args}>
    <img style={{justifySelf: 'center'}} src="https://via.placeholder.com/150" />
    <img style={{justifySelf: 'center'}} src="https://via.placeholder.com/150" />
    <img style={{justifySelf: 'center'}} src="https://via.placeholder.com/150" />
    <img style={{justifySelf: 'center'}} src="https://via.placeholder.com/150" />
    <img style={{justifySelf: 'center'}} src="https://via.placeholder.com/150" />
  </Grid>
</Wrapper>;

export const Small = Template.bind({});
Small.args = { cols: 1 }

export const Medium = Template.bind({});
Medium.args = { cols: 2 }

export const Large = Template.bind({});
Large.args = { cols: 3 }