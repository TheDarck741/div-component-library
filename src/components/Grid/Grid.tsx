import React, { ReactChildren, ReactNode } from "react";
import "./grid.css";

export interface GridProps {
  children:ReactNode;
  cols?: 1 | 2 | 3 ;
};

/**
 * Primary UI component for user interaction
 */
const Button = ({
  children,
  cols = 3,
}: GridProps) => {
  return (
    <div className={["grid", `grid--${cols}`].join(" ")}
    >
      {children}
    </div>
  );
};

export default Button;
