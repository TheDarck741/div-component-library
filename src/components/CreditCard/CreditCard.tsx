import React from "react";
import "./CreditCard.css";

export interface CreditCardProps {
    accountNumber: String,
    expirationDate: Date,
    solde: Number
} 


const CreditCard = ({
    accountNumber = "9999 9999 9999 9999",
    expirationDate = new Date(),
    solde = 1400.40
} : CreditCardProps) => {


    return (
        <div className="credit_card">
            <p className="accountNumber">{accountNumber}</p>
            <p className="expirationDate">{expirationDate.getMonth()}/{expirationDate.getFullYear()}</p>
            <p className="solde">{solde}€</p>
        </div>
    )
}

export default CreditCard;