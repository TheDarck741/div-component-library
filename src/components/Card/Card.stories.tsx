import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import Card, { CardProps } from "./Card";

export default {
  title: "Components/Layout/Card",
  component: Card
} as Meta;

// Create a master template for mapping args to render the Button component
const Template: Story<CardProps> = (args) => <Card {...args}> <h1>Titre test</h1> <p>Texte placeholder</p> <img src="https://via.placeholder.com/150" /> </Card>;

// Reuse that template for creating different stories
export const Rounded = Template.bind({});
Rounded.args = { rounded: true }

// Reuse that template for creating different stories
export const Square = Template.bind({});
Square.args = { rounded: false }