import React from "react"
import { Meta } from "@storybook/react/types-6-0"
import { Story } from "@storybook/react"
import Toaster, { ToasterProps } from './Toaster'

export default {
    title: 'Components/Toaster',
    component: Toaster
} as Meta

 const Template: Story<ToasterProps> = (args) => {
    return (
        <div>
            <Toaster 
                initToast={{
                    content: 'Une notification importante !',
                    backgroundColor:'#1ae8b7',
                    duration: 'long',
                    position: 'top right',
                    removeToast: () => {}
                }} { ...args }
            />
        </div>
    )
}
export const Default = Template.bind({})
