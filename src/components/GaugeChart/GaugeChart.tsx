import { Chart, ChartData, ChartOptions, registerables } from "chart.js"
import React, { useEffect, useState } from "react";
import { useRef } from "react";
import "./GaugeChart.css"

export interface GaugeChartProps {
    color: {
        red: number,
        green: number,
        blue: number,
        alpha?: number
    },
    label: string,
    percent: number 
}

const GaugeChart = ({
    color,
    label,
    percent
} : GaugeChartProps) => {

    const canvasRef = useRef<HTMLCanvasElement>(null)
    const [chartJsRef, setChartJsRef] = useState<Chart | null>(null)
    
    Chart.register(...registerables)

    useEffect(() => {
        if (canvasRef.current === null) return;

        if (chartJsRef !== null) {
            chartJsRef.destroy()
        }

        const chart = new Chart(canvasRef.current, {
            type: "doughnut",
            data: {
                labels: [label],
                datasets: [{
                    data: [
                        percent,
                        100 - percent,
                    ],
                    backgroundColor: [`rgba(${color.red}, ${color.green}, ${color.blue}, ${color.alpha ? color.alpha : 0.7})`, "rgb(62, 62, 62)"],
                    borderWidth: 0
                }]
            },
            options: {
                responsive: true,
                cutout: "90%",
                events: [],
                plugins: {
                    legend: {
                        display: false
                    }
                }
            }
        })
        setChartJsRef(chart);
    }, [color, label, percent])

    let classes = ["chart-gauge", "themeshadow-lg"]

    return (
        <div className={classes.join(" ")}>
            <canvas
            ref={canvasRef}>

            </canvas>
            <p className="label">{label}</p>
            <p className="percent">{percent}</p>
        </div>
    )
}

export default GaugeChart;