import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import Heading, { HeadingProps } from "./Heading";

export default {
  title: "Components/Basics/Heading",
  component: Heading,
} as Meta;

const Template: Story<HeadingProps> = (args) => <Heading {...args} />;

export const Default = Template.bind({});
Default.args = { level: 2, children: "Titre de base" };