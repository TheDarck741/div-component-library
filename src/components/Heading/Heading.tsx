import React from "react";
import "./Heading.css"

export interface HeadingProps {
    level: 1 | 2 | 3 | 4 | 5 | 6;
    children: React.ReactNode
}

const Heading = ({
    level = 2,
    children
} : HeadingProps) => {
    const Header = `h${level}` as keyof JSX.IntrinsicElements

    return (
        <Header 
        className="header">
            {children}
        </Header>
    )
}

export default Heading;