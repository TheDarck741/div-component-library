import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import TabNav, { TabProps } from "./TabNav";

export default {
  title: "Components/Tabnav",
  component: TabNav,
} as Meta;

// Create a master template for mapping args to render the Button component
const Template: Story<TabProps> = (args) => <TabNav {...args} />;

// Reuse that template for creating different stories
export const Primary = Template.bind({});
Primary.args = {
  chart: ()=> console.log('nav to chart'),
  card: ()=> console.log('nav to card'),
  notif: ()=> console.log('nav to notif'),
  menu: ()=> console.log('nav to menu')
};

// export const Secondary = Template.bind({});
// Secondary.args = { ...Primary.args, primary: false, label: "Secondary 😇" };
