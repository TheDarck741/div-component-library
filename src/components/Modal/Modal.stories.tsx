import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import Modale, { ModalProps }  from './Modal'

export default {
    title: 'Components/Modal',
    component: Modale
}

const Template: Story<ModalProps> = (args) => <Modale {... args} />

export const Default = Template.bind({})
