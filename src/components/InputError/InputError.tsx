import React from "react";
import "./InputError.css"

export interface InputErrorProps {
    errors?: string[]
}

const InputError = ({
    errors
}: InputErrorProps) => {
    
    let classes : string[] = ["input-error"];
    !errors ? classes.push("input-error-empty") : null;

    return  (
        <ul className={classes.join(" ")}>
            {errors && errors.map(el => <li>{el}</li>)}
        </ul>
    )

    
}

export default InputError;