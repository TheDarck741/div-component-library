import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import LineChart, { LineChartProps } from "./LineChart";

export default {
  title: "Components/Chart/LineChart",
  component: LineChart,
} as Meta;

const Template: Story<LineChartProps> = (args) => <LineChart {...args}></LineChart>;

const data = {
  labels: [1,2,3,4,5,6,7],
  datasets: [
    {
      label: 'Default',
      data: [30, 20, 20, 60, 60, 120, 180, 120, 125, 105, 110, 170],
      borderColor: "rgb(57, 211, 93)",
      fill: true,
      cubicInterpolationMode: 'monotone',
    }
  ]
};

export const Default = Template.bind({});
Default.args = { dataset: data, options: {
  responsive: true,
  elements: {
    line: {
      fill: true
    }
  },
  scales: {
    y: {
      display: false
    }
  },
  plugins: {
    legend: {
      display: false
    }
  }
},
color: {
  red: 57,
  green: 211,
  blue: 93,
}}