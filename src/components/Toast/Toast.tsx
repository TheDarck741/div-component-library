import React, { useState } from 'react'
import './Toast.css'

export interface ToastProps {
    content: string
    backgroundColor: string
    duration: 'infinite' | 'short' | 'medium' | 'long'
    position: 'top left' | 'top right' | 'bottom left' | 'bottom right'
    removeToast: (toast: ToastProps) => void
}

const Toast = ({
    content = 'Contenu du toast',
    backgroundColor = '#1ae8b7',
    duration = 'medium',
    position = 'bottom left',
    removeToast
}: ToastProps) => {
    let classes: string[] = []
    const props = {content, backgroundColor, duration, position, removeToast}
    switch (position) {
        case 'bottom left':
            classes.push('bottom-left')
            break
        case 'bottom right':
            classes.push('bottom-right')
            break
        case 'top left':
            classes.push('top-left')
            break
        case 'top right':
            classes.push('top-right')
            break
    }
    if (duration !== 'infinite') {
        let msDuration = 1000
        switch (duration) {
            case 'short':
                msDuration = 2000
                break
            case 'medium':
                msDuration = 5000
                break
            case 'long':
                msDuration = 10000
                break
        }
        setTimeout(() => {
            removeToast(props)
        }, msDuration)
    }
    return (
        <section className={'toast ' + classes.join(' ')} style={{background: backgroundColor}}>
            <p onClick={() => removeToast(props)}>{ props.content }</p>
        </section>
    )
}

export default Toast