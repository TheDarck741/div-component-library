import theme from '!!style-loader?injectType=lazyStyleTag!css-loader!../src/style/theme.css'
import cssVariablesTheme from '@etchteam/storybook-addon-css-variables-theme'

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  cssVariables: {
    files: {
      'Theme': theme,
    },
    defaultTheme: 'Theme'
  }
}

export const decorators = [
  cssVariablesTheme,
];
